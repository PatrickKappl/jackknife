#######
Modules
#######

The general implementation of jackknife resampling can be found in :mod:`jackknife`. The
other modules are for problem-specific extensions. They must provide a transformation
function :math:`f` as well as a generator for the input samples :math:`x_i`.

.. toctree::

   jackknife
   adga