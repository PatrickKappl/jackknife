#####################################
Welcome to jackknife's documentation!
#####################################

This project implements a general interface for jackknife resampling of one or more,
potentially high-dimensional observables. This is extended with problem-specifiy modules
and scripts. So far, they only exist for jackknifing :abbr:`DGA (dynamical vertex
approximation)` self-energies and :abbr:`DMFT (dynamical mean field theory)`
susceptibilites calculated with ADGA_. If you want to do exactly that, check out the user
documentation. If you are interested in the implementation details take a look at the
source code documentation. If you want to know how to build the documentation go to the
end of the page.

.. _ADGA: https://github.com/AbinitioDGA/ADGA

.. toctree::
   :maxdepth: 1
   :caption: User Documentation
   :name: user-documentation

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Source code documentation
   :name: source-code-documentation

   Modules
   Scripts


Building the documentation
**************************

The documentation you are currently reading including the user and source code
documentation is built with Sphinx_. It uses Python's docstrings_ and reStructuredText_.
To build it on your local machine you need to install Sphinx (e.g. with conda) and execute
the following commands:

.. code-block:: shell

   cd .../jackknife/doc
   make html

The generated HTML files can be found in :file:`.../jackknife/doc/build/html/`. The main
page you are currently looking at is called :file:`index.html`.

.. _Sphinx: http://www.sphinx-doc.org/en/master/
.. _docstrings: https://www.python.org/dev/peps/pep-0257/
.. _reStructuredText: https://docutils.sourceforge.io/rst.html
