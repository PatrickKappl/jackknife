#######
Scripts
#######

For every problem-specific extension module there should be a script named
``jackknife_<name_of_the_extension_module>``.

.. toctree::

   jackknife_adga