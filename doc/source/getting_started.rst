###############
Getting Started
###############

Welcome to *Getting Started*. This page is intended for users who want to jackknife DGA
self-energies and DMFT susceptibilities with the :mod:`jackknife_adga` script. It gives a
brief introduction of jackknife resampling and tells you how to install, configure and run
the code. If you are interested in the implementation details of the modules and scripts
please check out the :ref:`source code documentation <source-code-documentation>`.


Introduction
************

The jackknife is a resampling method that allows to estimate the bias, variance and other
statistics of a parameter. It is a very general method applicable to a wide range of
problems, hence the name.

In short, if you want to know :math:`y=f(x)`, where :math:`f` is a in general non-linear
but known function and you only have access to random samples of :math:`x`, you can
estimate :math:`y` with :math:`f(\bar{x})`. Here :math:`\bar{x}` denotes the sample mean
of the input samples :math:`x`. However, since :math:`f` can be non-linear this estimator
can carry a bias. With the help of jackknife resampling this bias can be reduced resulting
in a bias-corrected estimate for the observable :math:`y`. In addition it also allows to
estimate its variance, covariance and other common statistics.

In case of the :mod:`jackknife_adga` script the observables of interest :math:`y` are DGA
self-energies and DMFT susceptibilities. It is possible to jackknife multiple of them at
once. The non-linear function :math:`f` is the ADGA_ program and the random input samples
:math:`x` are DMFT Green's functions obtained with w2dynamics_. Since multiple input
samples are needed, you have to use worm sampling to generate the desired number of
2-particle Green's functions. The more worm samples you use, the better your statistics
will get.

.. _ADGA: https://github.com/AbinitioDGA/ADGA
.. _w2dynamics: https://github.com/w2dynamics/w2dynamics


Installation
************

:program:`jackknife` is written in Python 3, but should also work with Python 2.7 and
requires the following packages.

   - NumPy (1.16.2)
   - h5py (1.10.2)
   - configparser (3.7.4)

The numbers in parenthesis are not the minimum required version, but just one set of
versions that are tested and work. In addition

   - :program:`w2dynamics` and
   - :program:`ADGA`

must also be installed (instructions on how to do that are provided in their respective
documentation). :program:`ADGA` is the program that actually calculates the self-energies
and susceptibilities. :program:`w2dynamics` is a DMFT code used to calculate 1- and
2-particle Green's functions, which are the input quantities of :program:`ADGA`.


Usage
*****

The :mod:`jackknife_adga` script is relatively easy to use, but one needs to generate
quite a few input and config files with/for :program:`w2dynamics` and :program:`ADGA`
beforehand.

Input
=====

The following input and config files are necessary:

   - an :program:`ADGA` config file
   - a file containing the momentum resolved values of the tight-binding hamiltonian
     :math:`H(k)` (needed for :program:`w2dynamics` and :program:`ADGA`)
   - a file containing the 1-particle Green's function from :program:`w2dynamics`
   - a file containing *multiple* 2-particle Green's functions from :program:`w2dynamics`
     using worm sampling
   - a :program:`jackknife` config file

In the :program:`ADGA` config file the correct ``HkFile`` and ``1PFile`` must be
specified, but the ``2PFile`` and ``Outfile`` key will be set automatically by the script
according to the values given in the :program:`jackknife` config file. For further details
on the files regarding :program:`ADGA` and :program:`w2dynamics`, see their respective documentation.

jackknife config file
---------------------

An example of a :program:`jackknife` config file is provided in the code repository
(:download:`jackknife_adga.ini <../../jackknife_adga.ini>`). In the ``[General]`` section
there are three obligatory keys that every file must contain:

   - ``adga_root_directory``: the :file:`ADGA` directory which contains the
     :file:`scripts` and :file:`bin` folder
   - ``adga_config_file``: ususally called :file:`dga.conf`
   - ``two_particle_file``: the file containing the 2-particle Green's functions from
     :program:`w2dynamics`

More control is possible with the following optional keys:

   - ``output_file_prefix``: default value = ``jackknife``; the current date and time will
     be appended to this prefix of the output file
   - ``store_output_samples``: default value = ``no``; if set to ``yes``, the transformed
     bias-corrected ouput samples will also be stored in the output file (see
     :class:`~jackknife.Jackknife` for a definition of these)

In the ``[Observables]`` section the group name of at least one HDF5 dataset of the
:program:`ADGA` output file must be given. The keys of this section will be used as the
names of the observables in the :ref:`jackknife output file <Output>`.

.. note::
   See the documentation of `configparser.ConfigParser.getboolean()`_ for the possible
   values of boolean keys in the :program:`jackknife` config file.

.. _configparser.ConfigParser.getboolean():
   https://docs.python.org/3/library/configparser.html#configparser.ConfigParser.getboolean

Run
===

If all input and config files are available execute the following command to start the
actual calculation:

.. code-block:: shell

   python jackknife_adga.py [jackknife_config_file] [number_of_processes]

If the name of the :program:`jackknife` config file is not given, the default value
``jackknife_adga.ini`` will be used. If the number of processes is not specified, it is
set to ``1``.

.. note::
   The :mod:`jackknife_adga` script itself is not parallelized but :program:`ADGA` is
   called and executed with the given number of processes.

Output
======

The output is written to an HDF5 file named according to the ``output_file_prefix`` set in
the :ref:`jackknife config file` with the date and time of the calculation appended to it.
The exact structure is as follows::

   `output_file_prefix`_<YYYY-mm-dd_HH-MM-SS>.hdf5
         ├── .config
         │       ├── jk.n_samples
         │       ├── jk.store_output_samples
         │       ├── qmc.nmeas
         │       ├── adga.executable
         │       ├── adga.symmetrize_script
         │       ├── adga.two_particle_file
         │       ├── adga.n_processes
         │       ├── adga.<section>.<key>
         │       :
         ├── <name_of_observable_1>
         │       ├── mean
         │       ├── variance
         │       ├── standard_deviation
         │       ├── standard_error_of_mean
         │       ├── transformed_input_mean
         │       ├── covariance
         │       └── correlation
         ├── <name_of_observable_2>
         │       ├── mean
         :       :

For each observable given in the :program:`jackknife` config file there is a group of
datasets containing the jackknife estimator (``mean``) as well as some other common
statistics. The attributes of the group ``.config`` contain useful metadata including all
entries of the adga config file (``adga.<section>.<key>``).

.. seealso::
   :meth:`jackknife.Jackknife.write_results_to_file()`,
   :meth:`jackknife.Jackknife.do_estimation()` as well as
   :meth:`adga.Adga.write_config_data_to_file()` for a description of the datasets and attributes.
