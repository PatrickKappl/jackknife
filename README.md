# jackknife

General implementation of jackknife resampling with extension module and script for
jackknifing self-energies and susceptibilities calculated with
[AbinitioDGA](https://github.com/AbinitioDGA/ADGA).

**In any published papers arising from the use of jackknife, please cite:**

Patrick Kappl, Markus Wallerberger, Josef Kaufmann, Matthias Pickem, Karsten Held,  
Phys. Rev. B 102, 085124 (2020)  
https://doi.org/10.1103/PhysRevB.102.085124  
[arXiv:2005.04899](https://arxiv.org/abs/2005.04899)  
When using additional codes (like [w2dynamics](https://github.com/w2dynamics/w2dynamics)
and [ADGA](https://github.com/AbinitioDGA/ADGA)) in conjunction with **jackknife**, do not
forget to give credit to them as well.

For more information read the documentation: https://e138.git-pages.tuwien.ac.at/e138-01/software/jackknife/

## License

This project is licensed under the [GNU General Public License
v3](https://gitlab.com/PatrickKappl/jackknife/blob/master/LICENSE).
