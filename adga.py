# This file is part of the jackknife project, a general implementation
# of jackknife resampling with extension modules for self-energies and
# susceptibilites caculated with ADGA
# (https://github.com/AbinitioDGA/ADGA)
#
# The public repository can be found at
# https://gitlab.com/PatrickKappl/jackknife
#
# Copyright (C) <2019, 2020> <Patrick Kappl>
# E-mail address: kappl.pa@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import h5py
from shutil import copyfile
import numpy as np
import subprocess
import os
import sys
import time
from configparser import ConfigParser
import re


class Adga(object):
    """A class for AbinitioDGA calculations, to be used with
    :class:`~jackknife.Jackknife`.

    This class uses the `ADGA project`_ to calculate DMFT
    susceptibilities as well as the non-local self-energy in the
    dynamical vertex approximation. DMFT results for the 1- and
    2-particle Green's functions from w2dynamics_ are taken as input.

    .. _ADGA project: https://github.com/AbinitioDGA/ADGA
    .. _w2dynamics: https://github.com/w2dynamics/w2dynamics

    The location of the :file:`abinitiodga` executable and the
    :file:`symmetrize.py` script are assumed to be at
    :file:`<adga_root_path>/bin/abinitiodga` and
    :file:`<adga_root_path>/scripts/symmetrize.py` respectively. For
    jackknife resampling the 2-particle Green's function file must
    contain at least two worm samples.

    :arg dict dataset_names_of_observables:
        The values are names of datasets in the :program:`ADGA` output
        file specifying the observables that should be jackknifed. The
        keys are used as the names of these observables.
    """

    def __init__(self, adga_root_path, adga_config_file_name,
                 two_particle_greens_function_file_name,
                 dataset_names_of_observables, n_processes=1):
        self._n_processes = n_processes
        self._dataset_names_of_observables = dataset_names_of_observables
        self._executable = adga_root_path + "/bin/abinitiodga"
        self._symmetrize_script = adga_root_path + "/scripts/symmetrize.py"

        # Generate a unique id for the names of the auxiliary files.
        # Together with a unique copy of the config file, this allows to
        # run multiple jackknives at once.
        self._unique_id = int(time.time() * 1e6)
        self._output_file_name = "adga{}.hdf5".format(self._unique_id)
        g4iw_file_name = "g4iw{}.hdf5".format(self._unique_id)
        self._symmetry_file_name = "g4iw{}_sym.hdf5".format(self._unique_id)
        self._original_config_file_name = adga_config_file_name
        self._config_file_name = adga_config_file_name.split(".")[0] \
            + str(self._unique_id) + ".conf"

        try:
            copyfile(adga_config_file_name, self._config_file_name)
        except OSError:
            print("Could not copy the ADGA config file!")
            raise
        try:
            self._g2_file = h5py.File(two_particle_greens_function_file_name,
                                      "r")
        except IOError:
            print("""Could not open the file containing the 2-particle Green's
                  function!""")
            raise
        try:
            copyfile(two_particle_greens_function_file_name, g4iw_file_name)
        except OSError:
            print("""Could not copy the file containing the 2-particle Green's
                  function!""")
            raise
        self._g4iw_file = h5py.File(g4iw_file_name, "r+")
        try:
            self._worm_groups = [s + "/ineq-001/g4iw-worm/"
                                 for s in self._g2_file.keys()
                                 if (("worm-" in s) and (s != "worm-last"))]
            config = self._g2_file[".config"]
            self._n_qmc_measurements = config.attrs["qmc.nmeas"]
            self._n_worm_samples = config.attrs["general.wormsteps"]
            self._compound_indexes = list(
                self._g2_file[self._worm_groups[0]].keys())
        except Exception:
            print("""Could not read group names or config attributes from the
                  file containing the 2-particle Green's function!""")
            raise

    def __del__(self):
        """Delete all auxiliary files created during the calculation."""

        os.remove(self._output_file_name)
        os.remove(self._g4iw_file.filename)
        os.remove(self._symmetry_file_name)
        os.remove(self._config_file_name)

    @property
    def n_qmc_measurements(self):
        return self._n_qmc_measurements

    @property
    def n_worm_samples(self):
        return self._n_worm_samples

    def get_worm_sample_generator(self):
        """Return a generator yielding green's functions from different
        worm samples."""
        def worm_sample_generator():
            for worm_group in self._worm_groups:
                value_groups = [worm_group + i + "/value"
                                for i in self._compound_indexes]
                yield np.array([self._g2_file[value_group][...]
                                for value_group in value_groups])
        return worm_sample_generator

    def calculate_observables(self, two_particle_greens_function):
        """Calculate observables with :program:`ADGA` and return them.

        Symmetrize the given 2-particle Green's function and do the ab
        initio DGA calculation. From the :program:`ADGA` output file
        extract all observables given by their HDF5 dataset names. The
        returned object is a dictionary of these datasets with each of
        them being a numpy array. The keys are the same as those in
        `dataset_names_of_observables`.

        :arg two_particle_greens_function:
            obtained from generator returned by
            :meth:`get_worm_sample_generator()`
        """

        self._symmetrize_g2(two_particle_greens_function)
        self._do_dga_calculation()
        try:
            with h5py.File(self._output_file_name, "r") as output_file:
                observables = {key: output_file[dataset_name][...]
                               for key, dataset_name in
                               self._dataset_names_of_observables.items()}
        except IOError:
            print("Could not open ADGA output file!")
            raise
        return observables

    def write_config_data_to_file(self, file_path):
        """Write everything needed to configure :class:`Adga` to the
        given file.

        The following metadata is written as attributes to the
        ``.config`` group of the given HDF5 file (the group is expected
        to already exist):

            - ``qmc.nmeas``: number of QMC measurements per core when
              calculating the 2-particle Green's function (from
              :program:`w2dynamics`)
            - ``adga.executable``: path to the :program:`ADGA`
              executable
            - ``adga.symmetrize_script``: path to the
              :file:`symmetrize.py` script
            - ``adga.two_particle_file``: path to the file containing
              the 2-particle Green's function
            - ``adga.n_processes``: number of processes used for
              :program:`ADGA` calculations
            - all entries from the adga config file in the form
              ``adga.<section>.<key>``

        :arg file_path:
            path to the :program:`jackknife` output file with HDF5 group
            ``.config``
        """

        try:
            with h5py.File(file_path, "r+") as output_file:
                config = output_file[".config"].attrs
                config.create("qmc.nmeas",
                              np.string_(self._n_qmc_measurements))
                config.create("adga.executable", np.string_(self._executable))
                config.create("adga.symmetrize_script",
                              np.string_(self._symmetrize_script))
                config.create("adga.two_particle_file",
                              np.string_(self._g2_file.filename))
                config.create("adga.n_processes", self._n_processes)
                # Add everything from the adga config file
                config_parser = ConfigParser()
                config_parser.SECTCRE = re.compile(r"\[(?P<header>[^]].*)\]")
                config_parser.read(self._original_config_file_name)
                for section in config_parser.keys():
                    for key in config_parser[section].keys():
                        config.create("adga.{}.{}".format(section, key),
                                      np.string_(config_parser[section][key]))
        except IOError:
            print("Could not open jackknife output file!")
        return

    def _symmetrize_g2(self, g2):
        # Copy the two-particle Green's function to the g4iw file and
        # call symmetrize.py worm-001 g4iw_file.filename
        try:
            for i in range(len(self._compound_indexes)):
                data_set = self._g4iw_file["worm-001/ineq-001/g4iw-worm/"
                                           + self._compound_indexes[i]
                                           + "/value"]
                data_set[...] = g2[i]
            self._g4iw_file.flush()
        except Exception:
            print("Could not copy 2-particle Green's function to g4iw file!")
            raise

        # g4iw{}_sym.hdf5 must not exist when calling symmetrize.py
        if os.path.exists(self._symmetry_file_name):
            os.remove(self._symmetry_file_name)

        print(self._symmetrize_script + " worm-001 "
              + self._g4iw_file.filename)
        subprocess.call([self._symmetrize_script, "worm-001",
                         self._g4iw_file.filename])
        return

    def _do_dga_calculation(self):
        # Copy the config file, add the unique ID and set the correct
        # 2-particle and output file. This allows to run multiple
        # jackknives at the same time without interference.
        self._set_value_in_config_file("Two-Particle", "2PFile",
                                       self._symmetry_file_name)
        self._set_value_in_config_file("General", "Outfile",
                                       self._output_file_name)

        # Do the DGA calculation
        print("mpirun" + " -np " + str(self._n_processes) + " "
              + self._executable + " " + self._config_file_name)
        try:
            subprocess.check_call(["mpirun", "-np", str(self._n_processes),
                                   self._executable, self._config_file_name])
        except Exception:
            print("""Something went wrong while running the abinitio DGA
                  executable!""")
            raise
        return

    def _set_value_in_config_file(self, section, key, value):
        # Replace or add the key-value pair in the given section of the
        # config file
        file_name = self._config_file_name
        new_line = key + " = " + value + "\n"

        lines = open(file_name).readlines()
        line_is_in_section = False
        for line in lines:
            section_is_found = line.startswith("[" + section + "]")
            key_is_found = line_is_in_section and line.startswith(key)
            end_of_section_is_found = line and line_is_in_section \
                and line[0] == "[" and line[1].isalpha()
            if section_is_found:
                line_is_in_section = True
                continue
            elif key_is_found:
                # Replace the key-value pair with the new one
                lines[lines.index(line)] = new_line
                break
            elif end_of_section_is_found:
                # Add the key-value pair at the end of the section
                lines.insert(lines.index(line) - 1, new_line)
                break

        with open(file_name, "w") as config_file:
            for line in lines:
                config_file.write(line)
        return
