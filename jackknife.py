# -*- coding: utf-8 -*-

# This file is part of the jackknife project, a general implementation
# of jackknife resampling with extension modules for self-energies and
# susceptibilites caculated with ADGA
# (https://github.com/AbinitioDGA/ADGA)
#
# The public repository can be found at
# https://gitlab.com/PatrickKappl/jackknife
#
# Copyright (C) <2019, 2020> <Patrick Kappl>
# E-mail address: kappl.pa@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import h5py
import time
import sys


class Jackknife(object):
    """A class implementing a jackknife estimation.

    The jackknife is a resampling method that allows to estimate the
    variance and bias of a parameter. This can further be used to obtain
    the bias-corrected jackknife estimator.

    Given :math:`n` input samples :math:`x_i` and a transformation
    function :math:`f()`, the goal is to estimate :math:`y=f(x)`, with
    :math:`x` and :math:`y` being the true values. A simple estimate
    would just be the transformed input mean :math:`f(\\bar{x})`, with
    :math:`\\bar{x}` being the input sample mean. In general this
    carries a bias of order :math:`1/n`. Using jackknife resampling this
    leading :math:`1/n` term can be estimated and removed, yielding the
    bias-corrected jackknife estimator.

    First, the resampling of the input is done by

    .. math::
        x_i \\rightarrow x'_i = (n\\bar{x} - x_i) / (n-1),

    where :math:`x'_i` is the new sample. This is then transformed in
    the following way:

    .. math::
        y_i = n f(\\bar{x}) - (n-1) f(x'_i),

    where :math:`y_i` is the transformed, bias-corrected output sample.
    The jackknife estimator :math:`\\hat{y}_{\\textrm{jackknife}}` can
    now be obtained simply by calculating the sample mean of the
    :math:`y_i`. Other statistical quantities like the variance and
    covariance for example can also be calculated from the output
    samples. For more details on that, see :meth:`do_estimation`.

    Both :math:`x_i` and :math:`y_i` can be scalar or high-dimensional
    quantities. To allow jackknifing multiple observables at once, the
    output samples must be a dictionary of numbers or numpy arrays,
    where each entry represents a different observable. The input
    samples are either numbers or numpy arrays. In the latter case,
    naturally, all calculations are done element-wise.

    :var input_sample_generator_function:
        Function that returns a generator yielding the input samples
        :math:`x_i`. An input sample must either be a number or a numpy
        array.
    :var int n_samples:
        :math:`n`
    :var transformation_function:
        :math:`f: x_i\\rightarrow y_i`. Callable that takes a single
        input sample :math:`x_i` as an argument and returns an output
        sample :math:`y_i`. An output sample must be a dictionary of
        numbers or numpy arrays. The keys in this dictionary are used as
        names for the observables in the output file (see
        :meth:`write_results_to_file()`) and the number of observables
        is deduced from the dictionary's length.
    :var correlation_axis:
        Axis along which the outer product for the covariance and
        correlation matrix is calculated. Must be an integer or an
        iterable with a length equal to the number of observables.
    :var output_file_prefix:
    :var stores_output_samples:
        If ``True`` the output samples :math:`y_i` are also stored in
        the HDF5 output file.

    :raise ValueError: if :math:`n<2`
    """

    def __init__(self, input_sample_generator_function, n_samples,
                 transformation_function, correlation_axis=-1,
                 output_file_prefix="", output_samples_are_stored=False):
        self.input_sample_generator_function = input_sample_generator_function
        self.n_samples = n_samples
        if n_samples < 2:
            raise ValueError("""At least 2 samples are needed for the jackknife
                             estimation.""")
        self.transformation_function = transformation_function
        self.correlation_axis = correlation_axis
        self.output_file_prefix = output_file_prefix
        self.stores_output_samples = output_samples_are_stored

        self._n_observables = None
        self._observables_names = None
        self._date_and_time = ""
        self._output_file_name = ""
        self._input_mean = None
        self._transformed_mean = None
        self._output_mean = None
        self._output_variance = None
        self._output_standard_deviation = None
        self._output_standard_error_of_mean = None
        self._output_covariance = None
        self._output_correlation = None
        self._output_samples = []

    @property
    def output_file_name(self):
        """:file:`\\`output_file_prefix\\`_<YYYY-mm-dd_HH-MM-SS>.hdf5`"""
        return self._output_file_name

    def do_estimation(self):
        """Estimate :math:`y=f(x)` and some common statistics.

        The jackknife estimator :math:`\\hat{y}_{\\textrm{jackknife}}`
        is simply given by the sample mean :math:`\\bar{y}`,

        .. math::
            \\hat{y}_{\\textrm{jackknife}} = \\bar{y} = \\frac{1}{n}
                \\sum_i y_i,

        with output samples :math:`y_i` and sample size :math:`n`. Other
        common statistical quantities are estimated with the sample
        variance :math:`s^2`, the sample standard deviation :math:`s`,
        the standard error of the mean :math:`\\textrm{SEM}`, the sample
        covariance matrix :math:`q` and the matrix of sample Pearson
        correlation coefficients :math:`r`. They are calculated using
        the following formulas:

            .. math::
                s^2 &= \\frac{1}{n-1}\\left(\\sum_i y_i^2 - \\frac{1}{n}
                    \\left(\\sum_i y_i\\right)^2\\right),\\\\
                s &= \\sqrt{s^2},\\\\
                \\textrm{SEM} &= \\frac{s}{\\sqrt{n}},\\\\
                q &= \\frac{1}{n-1} \\left(\\sum_i y_i\\otimes y_i -
                    \\frac{1}{n}\\left(\\sum_i y_i\\right) \\otimes
                    \\left(\\sum_i y_i\\right)\\right),\\\\
                r_{ij} &= \\frac{q_{ij}}{s_i s_j}.

        Here, :math:`\\otimes` denotes the outer product along the
        :attr:`correlation_axis`. Since :math:`y` is implemented as a
        dictionary potentially holding multiple independent observables,
        naturally, the above statistics are calculated separately for
        each entry.

        .. note::
            While the sample variance :math:`s^2` is an unbiased
            estimator, the sample standard deviation :math:`s` is not.
            It generally underestimates the standard deviation. Because
            of that, also the standard error of the mean is
            underestimated.
        """

        start = time.time()
        print("-- Begin jackknife estimation")
        self._date_and_time = time.strftime("%Y-%m-%d_%H-%M-%S")

        get_samples = self.input_sample_generator_function
        f = self.transformation_function
        n = self.n_samples

        print("-- Calculating transformed input mean ...")
        self._input_mean = sum(i for i in get_samples()) / n
        self._transformed_mean = f(self._input_mean)
        self._observables_names = list(self._transformed_mean.keys())
        self._n_observables = len(self._observables_names)
        self._transformed_mean = list(self._transformed_mean.values())
        if isinstance(self.correlation_axis, int):
            self.correlation_axis = [self.correlation_axis] \
                * self._n_observables
        elif len(self.correlation_axis) != self._n_observables:
            raise ValueError("`correlation_axis` must be an integer or an"
                             + " iterable with a length equal to the number of"
                             + " observables!")

        # To reduce round-off errors when calculating the sample
        # variance, the sample mean should be close to zero. Therefore
        # all samples should be shifted by it. Since the mean is not
        # known thus far, it is estimated with a random (in this case
        # the first) y value.
        print("-- Calculating shift ...")
        samples = get_samples()
        shift = self._resample_and_transform(next(samples))
        sum_ = [0] * self._n_observables
        sum_of_squares = [0] * self._n_observables
        sum_of_outer_products = [0] * self._n_observables
        i_sample = 1
        for sample in samples:
            print("-- Calculating sample {} ...".format(i_sample))
            y = self._resample_and_transform(sample)
            if self.stores_output_samples:
                self._output_samples.append(y)
            for i in range(self._n_observables):
                y[i] -= shift[i]
                sum_[i] += y[i]
                sum_of_squares[i] += np.abs(y[i])**2
                sum_of_outer_products[i] += \
                    self._calculate_outer_product(y, y, i)
            i_sample += 1

        self._calculate_statistics(shift, sum_, sum_of_squares,
                                   sum_of_outer_products)
        end = time.time()
        print("\n-- The jackknife estimation took " + str(end - start) + "s.")
        return

    def write_results_to_file(self):
        """Write the results of the last jackknife estimation to a file.

        Create an HDF5 file with the following structure::

            `output_file_prefix`_<YYYY-mm-dd_HH-MM-SS>.hdf5
                    ├── .config
                    │       ├── jk.n_samples
                    │       └── jk.store_output_samples
                    ├── <name_of_observable_1>
                    │       ├── mean
                    │       ├── variance
                    │       ├── standard_deviation
                    │       ├── standard_error_of_mean
                    │       ├── transformed_input_mean
                    │       ├── covariance
                    │       └── correlation
                    ├── <name_of_observable_2>
                    │       ├── mean
                    :       :

        The time stamp is from the start of the last jackknife
        estimation and the keys of the dictionary returned by
        :attr:`transformation_function` are used for the names of the
        observables.

        .. Note::
            User code can and should expand the ``.config`` group with
            problem-specific metadata, e.g., with information about the
            input samples or the transformation function.
        """

        self._output_file_name = self.output_file_prefix + "_"
        self._output_file_name += self._date_and_time + ".hdf5"
        output_file = h5py.File(self._output_file_name, "w")
        output_file.create_group(".config")
        output_file[".config"].attrs.create("jk.n_samples", self.n_samples)
        output_file[".config"].attrs.create("jk.stores_output_samples",
                                            self.stores_output_samples)

        for i in range(self._n_observables):
            group = output_file.create_group(self._observables_names[i])
            group.create_dataset("mean", data=self._output_mean[i])
            group.create_dataset("variance", data=self._output_variance[i])
            group.create_dataset("standard_deviation",
                                 data=self._output_standard_deviation[i])
            group.create_dataset("standard_error_of_mean",
                                 data=self._output_standard_error_of_mean[i])
            group.create_dataset("transformed_input_mean",
                                 data=self._transformed_mean[i])
            group.create_dataset("covariance",
                                 data=self._output_covariance[i])
            group.create_dataset("correlation",
                                 data=self._output_correlation[i])

            if self.stores_output_samples:
                subgroup = group.create_group("output_samples")
                i_sample = 0
                for x in self._output_samples:
                    subgroup.create_dataset(str(i_sample), data=x[i])
                    i_sample += 1
        output_file.close()
        return

    def _resample_and_transform(self, sample):
        # Do a resampling and a bias-corrected transformation of the
        # given sample and return it.
        n = self.n_samples
        resample = (n * self._input_mean - sample) / (n - 1)
        f_resample = list(self.transformation_function(resample).values())
        return [n * self._transformed_mean[i] - (n - 1) * f_resample[i]
                for i in range(self._n_observables)]

    def _calculate_outer_product(self, array_list_1, array_list_2,
                                 i_observable):
        # Calculate and return the outer product between 2 arrays
        # a=array_list_1[i_observable] and b=array_list_2[i_observable]
        # along the `correlation_axis` according to the following
        # formular: c_{...,i,j,...}=a_{...,i,...}b_{...,j,...}^*,
        axis = self.correlation_axis[i_observable]
        a = np.moveaxis(array_list_1[i_observable], axis, -1)
        b = np.moveaxis(array_list_2[i_observable], axis, -1)
        result = a[..., :, np.newaxis] * np.conj(b[..., np.newaxis, :])
        result = np.moveaxis(result, -1, axis)
        result = np.moveaxis(result, -1, axis)
        return result

    def _calculate_statistics(self, shift, sum_, sum_of_squares,
                              sum_of_outer_products):
        # Calculate the output mean, variance, standard deviation,
        # standard error of the mean, covariance and correlation.
        n = self.n_samples

        # Add the shift back again to get the correct output mean
        self._output_mean = [sum_[i] / n + shift[i]
                             for i in range(self._n_observables)]
        # The sample variance is invariant under shifts -> no extra
        # stuff to do
        self._output_variance = [
            (sum_of_squares[i] - np.abs(sum_[i])**2 / n) / (n - 1)
            for i in range(self._n_observables)]
        self._output_standard_deviation = [np.sqrt(self._output_variance[i])
                                           for i in range(self._n_observables)]
        self._output_standard_error_of_mean = [
            self._output_standard_deviation[i] / np.sqrt(n)
            for i in range(self._n_observables)]
        self._output_covariance = [
            (sum_of_outer_products[i]
             - self._calculate_outer_product(sum_, sum_, i) / n) / (n - 1)
            for i in range(self._n_observables)]
        self._output_correlation = [
            self._output_covariance[i]
            / self._calculate_outer_product(self._output_standard_deviation,
                                            self._output_standard_deviation, i)
            for i in range(self._n_observables)]
        return
