#!/usr/bin/env python

# This file is part of the jackknife project, a general implementation
# of jackknife resampling with extension modules for self-energies and
# susceptibilites caculated with ADGA
# (https://github.com/AbinitioDGA/ADGA)
#
# The public repository can be found at
# https://gitlab.com/PatrickKappl/jackknife
#
# Copyright (C) <2019, 2020> <Patrick Kappl>
# E-mail address: kappl.pa@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: Use click for command line arguments and --help

from jackknife import Jackknife
from adga import Adga
import numpy as np
import sys
import h5py
import configparser


def main():
    """Do jackknife resampling for :program:`ADGA` calculations.

    Determine the config file and number of processes to run
    :program:`ADGA` with from the command line arguments. If all or some
    arguments are missing, use the default values. They are
    ``jackknife_adga.ini`` for the config file and ``1`` for the number
    of processes. Read the config file and check if obligatory keys are
    missing. If not, perform a jackknife estimation of the observables
    chosen in the config file. The results as well as some useful
    metadata are stored in an HDF5 file.

    :raise KeyError:
        if an obligatory key is missing in the config file
    :raise SystemExit:
        if no observables are given in the config file

    .. seealso::
        :ref:`Jackknife config file` and :ref:`Output`
    """

    config_file_name = "jackknife_adga.ini"
    n_processes = 1
    if len(sys.argv) == 1:
        print("No configuration file specified. Default is "
              + config_file_name)
    if len(sys.argv) >= 2:
        config_file_name = sys.argv[1]
    if len(sys.argv) >= 3:
        n_processes = int(sys.argv[2])
    if len(sys.argv) > 3:
        print("""Too many command line arguments. Please specify only the
              jackknife configuration file and the number of processes.
              Everything after that will be ignored.""")

    general, observables = _read_config_file(config_file_name)

    adga = Adga(general["adga_root_directory"], general["adga_config_file"],
                general["two_particle_file"], observables, n_processes)
    x_generator = adga.get_worm_sample_generator()
    n = adga.n_worm_samples
    f = adga.calculate_observables

    # Do the Jackknife estimation
    jackknife = Jackknife(x_generator, n, f, -1,
                          general["output_file_prefix"],
                          general["store_output_samples"])
    jackknife.do_estimation()
    jackknife.write_results_to_file()
    adga.write_config_data_to_file(jackknife.output_file_name)
    return


def _read_config_file(config_file_name):
    # Read and return general config data and observables from the given
    # file.
    #
    # :raise KeyError: if an obligatory key is missing in the config
    #                  file
    # :raise SystemExit: if no observables are given in the config file

    # Define the dictionary for the [General] section of the INI file.
    # Obligatory keys are set to `None`.
    general = {"adga_root_directory": None,
               "adga_config_file": None,
               "two_particle_file": None,
               "output_file_prefix": "jackknife",
               "store_output_samples": False}

    # Read config file and check if obligatory keys are missing
    config = configparser.ConfigParser()
    config.read(config_file_name)
    for key in config["General"]:
        try:
            if isinstance(general[key], bool):
                general[key] = config["General"].getboolean(key)
            else:
                general[key] = config["General"][key]
        except KeyError:
            print("Warning: unsupported key \"{}\" found in".format(key)
                  + " config file will be ignored.")
    for key in general:
        if general[key] is None:
            raise KeyError("Error while parsing conifg file.\n"
                           + "Obligatory key \"{}\" not found.".format(key))

    # Get all key-value pairs of the [Observables] section and exit if
    # there are none
    observables = {}
    for key in config["Observables"]:
        observables[key] = config["Observables"][key]
    if len(observables) == 0:
        raise SystemExit("No Observables were given. Since you don't want to"
                         + " calculate anything we're done.")

    return general, observables


if __name__ == "__main__":
    main()
